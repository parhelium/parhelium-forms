Package.describe({
  name: 'parhelium:forms',
  summary: '',
  version: '1.0.0',
  git: ' '
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.2.1');

  api.use([
    "less",
    'templating',
    'jquery',
    'underscore'
  ],['client']);
  
  
  api.addFiles([
      'lib/antiForm.html',
      'lib/closestData.js',
      'lib/forms.js',
      'lib/underscoreExtensions.js',
      'lib/fields/_.js',
      'lib/fields/_helpers.js',
      'lib/fields/singleSelection.html',
      'lib/fields/singleSelection.js',
      'lib/fields/singleSearchSelection.html',
      'lib/fields/errorDescription.html',
      'lib/fields/numberField.html',
      'lib/fields/radioButtons.html',
      'lib/fields/radioButtons.js',
      'lib/fields/segment.html',
      'lib/fields/switch.html',
      'lib/fields/textArea.html',
      'lib/fields/textField.html',
      'lib/fields/textFieldPassword.html',
      'lib/fields/textTwoFields.html',
      'lib/fields/toggle.html',
      'lib/fields/toggle.js'
    ],    ['client']  )
});

