
Template.antiForm_radioButtons.events({
  'click .button': function(e, t) {
    var checkbox = t.$('input[value=' + this.value + ']');
    checkbox.prop("checked", true);

    t.$('.button').removeClass('checked');
    $(e.currentTarget).addClass('checked');
  }

});

