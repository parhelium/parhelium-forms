var helpers = {
  anf_checked: function() {
    if(!this.name) return;
    var doc = Template.closestData('antiFormDoc').antiFormDoc;
    return _.valueAtKey(doc, this.name) ? 'checked': '';
  },
  
  anf_radioChecked: function() {
    var field = Template.closestData('name');
    var doc = Template.closestData('antiFormDoc').antiFormDoc
    return (_.valueAtKey(doc, field.name) == this.value) ? 'checked': '';
  },

  anf_value: function() {
    if(!this.name) return;
    var doc = Template.closestData('antiFormDoc').antiFormDoc
    console.log('anf_value,', this.name, '\ndoc=',doc)
    return _.valueAtKey(doc, this.name);
  },

  anf_error: function() {
    if(!this.name) return;
    var error = Template.closestData('antiFormError').antiFormError
    return _.valueAtKey(error, this.name);
  }
};



_.each(helpers, function(helper, key) {
  Template.registerHelper(key, helper)
});

