AntiForm = {};

AntiForm.validateData = function(data, schema, errorVariable) {

  var error = {};
  var hasError = false;

  _.each(schema, function(requirement, key) {
    var value = _.valueAtKey(data, key);

    if(requirement === 'string') {
      if(!_.isNonBlankString(value)) {
        hasError = true;
        error[key] = "Required field.";
      }
      return;
    }

    if(requirement === 'number') {
      if(!_.isNumber(value)) {
        if(typeof value === 'number') return;
        if(typeof value === 'string' && /^\-?\d+\.?\d*$/.test(value)) return;
        hasError = true;
        error[key] = "Required field."; 
      }
    }
    
  });

  errorVariable.set(error);
  return !hasError;
};

