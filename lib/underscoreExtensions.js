_.indexWhere = function(array, condition) {
  for(var idx = 0; idx < array.length; ++idx) {
    var ok = true;
    _.each(condition, function(v, k) {
      if(array[idx][k] !== v) ok = false;
    });
    if(ok) return idx;
  }
  return -1;
};


_.valueAtKey = function(object, key) {
  if(!key) return null;
  // console.log("============================================================");
  // console.log("VAK", object, key);
  var keys = key.split('.');
  if(keys.length === 0) return null;
  var o = object;
  for(var i = 0; i < keys.length; ++i) {
    if(!o) return null;
    o = o[keys[i]];
  }
  return o;
};

_.trimString = function(string) {
  return string.replace(/(^\s+|\s$)/g, '');
};

_.isNonBlankString = function(string) {
  if(typeof string !== 'string') return false;
  if(_.trimString(string).length === 0) return false;
  return true;
};